<%-- 
    Document   : liste
    Created on : 7 nov. 2022, 14:44:37
    Author     : ASUS
--%>

<%@page import="java.util.Vector" %>
<%@page import="Modele.Personne" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% Vector<Personne> v = (Vector<Personne>) request.getAttribute("liste"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Age</th>
                <th>Departement</th>
                
            </tr>
            <% for(int i=0; i<v.size(); i++) { 
                Personne p = (Personne) v.elementAt(i);
            %>
                <tr>
                    <td><%= p.getNom() %></td>
                    <td><%= p.getPrenom() %></td>
                    <td><%= p.getAge() %></td>
                    <td><%= p.getNomDept().getNomDept() %></td>
                </tr>
            <% }%>
        </table>
        <p><a href="index.jsp"><button>Retour</button></a></p>
    </body>
</html>
