/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import MyAnnotation.MyAnnotation;
import Utilitaire.ModelView;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author ASUS
 */
public class Personne {
    private String nom;
    private String prenom;
    private Integer age;
    private Dept nomDept;

    public Personne() {
    }

    public Personne(String nom, String prenom, Integer age, Dept nomDept) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.nomDept = nomDept;
    }

    
    public Dept getNomDept() {
        return nomDept;
    }

    public void setNomDept(Dept nomDept) {
        this.nomDept = nomDept;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    
    
    @MyAnnotation(url="Personne-list")
    public ModelView lister(){
        ModelView mv = new ModelView();
        HashMap<String, Object> data = new HashMap<String, Object>();
        Vector<Personne> liste = new Vector<Personne>();
        
        Dept d1 = new Dept(1, "Dept1");
        Dept d2 = new Dept(2, "Dept2");
        Dept d3 = new Dept(3, "Dept3");
        
        Personne p1 = new Personne("Razafy", "Hery", 24, d1);
        Personne p2 = new Personne("Rabe", "Eric", 20, d2);
        Personne p3 = new Personne("Andria", "Mirana", 22, d3);
        
        liste.add(p1);
        liste.add(p2);
        liste.add(p3);

        data.put("liste", liste);
        
        mv.setUrl("liste.jsp");
        mv.setData(data);
        
        return mv;
    }
    
    @MyAnnotation(url="Personne-save")
    public ModelView save() {
        ModelView mv = new ModelView();
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("nom", this.getNom());
        data.put("prenom", this.getPrenom());
        data.put("age", this.getAge());
        data.put("dept", this.getNomDept().getNomDept());
        
        mv.setUrl("accueil.jsp");
        mv.setData(data);

        return mv;
    }
    
    @MyAnnotation(url = "Personne-ajout")
    public ModelView ajout() {
        ModelView mv = new ModelView();
        mv.setUrl("formulaire.jsp");
        
        return mv;
    }
}
