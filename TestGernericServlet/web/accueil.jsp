<%-- 
    Document   : accueil
    Created on : 18 nov. 2022, 14:06:38
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>Nom: <%= request.getAttribute("nom") %> </p>
        <p>Prenom(s): <%= request.getAttribute("prenom") %></p>
        <p>Age: <%= request.getAttribute("age") %> </p>
        <p>Departement: <%= request.getAttribute("dept") %></p>
        <p><a href="index.jsp"><button>Retour</button></a></p>
    </body>
</html>
